import re
import pandas as pd
import requests
import spacy
import networkx as nx
import os
import matplotlib.pyplot as plt
from spacy.matcher import Matcher
import en_core_web_lg


def get_from_wiki(title):
    """
    Gets a text from Wikipedia by name, used for testing
    :param title: title of Wikipedia Article
    :return: list of sentences
    """
    page_get = requests.get(
        "http://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&titles={}&redirects=true".format(
            title))
    page_id = list(page_get.json().get("query").get("pages").keys())[0]
    page_text = page_get.json().get("query").get(
        "pages").get(page_id).get("extract")
    return page_text


def clean_text(text):
    """
    Takes text or webpage and cleans it:
        - Removes HTML
        - Removes \n
        - Separates by punctuation
    :param text: test, String
    :return: list of sentectes
    """
    text_cleaner = re.compile('<.*?>')
    text_clean = re.sub(text_cleaner, '', text)

    text_array = text_clean.replace("\n", "")
    text_array = [t for t in text_array.split(".")]

    # Cleaning Spaces
    for i in range(0, len(text_array) - 1):
        if (text_array[i].startswith(" ")):
            text_array[i] = text_array[i][1:]

    for i in range(0, len(text_array) - 1):
        if (text_array[i].startswith(" ")):
            text_array[i] = text_array[i][1:]

    return text_array


def clean_df(df):
    """
    Makes some small adjustments to KG DF:
        - Removes incomplete pairs
        - Replaces it with subject of sentences before.
    :param df: KG DF to be worked on, Columns: subject, object, relation
    :return: f: KG DF, Columns: subject, object, relation
    """
    df = df[~(df['object'] == '')]
    df = df[~(df['subject'] == '')]
    for index, row in df.iterrows():
        if row['subject'].lower() == "it":
            df['subject'][index] = df['subject'][index - 1]
    return df


def add_relation_to_main(name, df, relation_name):
    """
    Adds for each subject, that is not name a relation to name with relation_name:
    :param relation_name: name of the relation to name
    :param name: name to add relations to
    :param df: kg df, Columns: subject, object, relation
    :return: KG DF, Columns: subject, object, relation
    """
    for index, row in df.iterrows():
        if not row['subject'].lower() == name.lower():
            df = df.append({'subject': name, 'object': row.subject,
                            'relation': relation_name}, ignore_index=True)
    return df


class Text2KG:
    def __init__(self, link_to_name=False, link_to_name_relation_name="has property"):

        # todo move all spacy to text2kg
        self.nlp = en_core_web_lg.load()
        self.link_to_name = link_to_name
        self.link_to_name_relation_name = link_to_name_relation_name

    def get_entities(self, sent):
        """
        Gets Subject and Object from given sentence.
        Inspired by function from https://www.analyticsvidhya.com/blog/2019/10/how-to-build-knowledge-graph-text-using-spacy/

        :param sent: Sentence to get subject and object from
        :return: list of subject [0] and object [1]
        """
        # chunk 1
        ent1 = ""
        ent2 = ""

        prv_tok_dep = ""  # dependency tag of previous token in the sentence
        prv_tok_text = ""  # previous token in the sentence

        prefix = ""
        modifier = ""

        #############################################################

        for tok in self.nlp(sent):
            # chunk 2
            # if token is a punctuation mark then move on to the next token
            if tok.dep_ != "punct":
                # check: token is a compound word or not
                if tok.dep_ == "compound":
                    prefix = tok.text
                    # if the previous word was also a 'compound' then add the current word to it
                    if prv_tok_dep == "compound":
                        prefix = prv_tok_text + " " + tok.text

                # check: token is a modifier or not
                if tok.dep_.endswith("mod"):
                    modifier = tok.text
                    # if the previous word was also a 'compound' then add the current word to it
                    if prv_tok_dep == "compound":
                        modifier = prv_tok_text + " " + tok.text

                # chunk 3
                if tok.dep_.find("subj"):
                    ent1 = modifier + " " + prefix + " " + tok.text
                    prefix = ""
                    modifier = ""
                    prv_tok_dep = ""
                    prv_tok_text = ""

                    # chunk 4
                if tok.dep_.find("obj"):
                    ent2 = modifier + " " + prefix + " " + tok.text

                # chunk 5
                # update variables
                prv_tok_dep = tok.dep_
                prv_tok_text = tok.text
        #############################################################

        return [ent1.strip(), ent2.strip()]

    def get_relation(self, sent):
        """
        Gets Relation (Verb) from given sentence.
        Inspired by function from https://www.analyticsvidhya.com/blog/2019/10/how-to-build-knowledge-graph-text-using-spacy/
        :param sent: Sentence to get verb from
        :return: verb
        """
        doc = self.nlp(sent)
        # Matcher class object
        matcher = Matcher(self.nlp.vocab)

        # define the pattern
        pattern = [
            {'DEP': 'ROOT'},
            # {'DEP':'attr','OP':'+'},
            {'DEP': 'prep', 'OP': "?"},
            # {'DEP':'agent','OP':"?"},
            # {'POS':'ADJ','OP':"?"}
        ]

        matcher.add("matching_1", None, pattern)

        matches = matcher(doc)
        k = len(matches) - 1

        span = doc[matches[k][1]:matches[k][2]]

        return span.text

    def get_kg_df_from_text(self, text, title=None):
        text_list = clean_text(text)
        entity_pairs_relations_dict = [{"entity_pairs": self.get_entities(i), "relations": self.get_relation(i)} for i
                                       in text_list]
        # extract subject
        source = [d.get("entity_pairs")[0]
                  for d in entity_pairs_relations_dict]
        # extract object
        target = [d.get("entity_pairs")[1]
                  for d in entity_pairs_relations_dict]
        # extract relations
        relations = [d.get("relations") for d in entity_pairs_relations_dict]

        relation_df = pd.DataFrame(
            {'subject': source, 'object': target, 'relation': relations})

        relation_df = clean_df(relation_df)

        if self.link_to_name:
            if title is None:
                print(
                    "Set link_to_name=True without giving a title to link to.\nSkipping this step.")
            else:
                relation_df = add_relation_to_main(
                    title, relation_df, self.link_to_name_relation_name)

        return relation_df

    def get_solution_answer(self, solution_text, answer_text):
        solution_kg = self.get_kg_df_from_text(solution_text)
        answer_kg = self.get_kg_df_from_text(answer_text)
        return solution_kg, answer_kg


def draw_kg(pairs):
    """
    A helper to display the KG as Graph.
    Function inspired by https://towardsdatascience.com/auto-generated-knowledge-graphs-92ca99a81121
    :param pairs: Dataframe, Columns: subject, object, relation
    :return: None, displays graph
    """
    k_graph = nx.from_pandas_edgelist(pairs, 'subject', 'object',
                                      create_using=nx.MultiDiGraph())
    node_deg = nx.degree(k_graph)
    layout = nx.spring_layout(k_graph, k=0.15, iterations=20)
    plt.figure(num=None, figsize=(30, 30), dpi=300)
    nx.draw_networkx(
        k_graph,
        node_size=[int(deg[1]) * 500 for deg in node_deg],
        arrowsize=20,
        linewidths=1.5,
        pos=layout,
        edge_color='red',
        edgecolors='black',
        node_color='white',
    )
    labels = dict(zip(list(zip(pairs.subject, pairs.object)),
                      pairs['relation'].tolist()))
    nx.draw_networkx_edge_labels(k_graph, pos=layout, edge_labels=labels,
                                 font_color='red')
    plt.axis('off')
    plt.show()


if __name__ == '__main__':
    # Main function for test purposes.
    article_name = "Wikipedia"  # "DNA"#"Wikipedia"
    example_sentences = get_from_wiki(article_name)[:50]
    text2kg = Text2KG()
    df = text2kg.get_kg_df_from_text(example_sentences)
    print(df)
