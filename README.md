# nlp_main

Alexa Question Answering Bot with Knowledge Graph
# Development
## Installation
```bash
virtualenv --python=/usr/bin/python3 .env
source .env/bin/activate
pip install -r requirements.txt
```
## During development
- When installing new packages use venv and pip to install and do `pip freeze > requirements.txt` afterwards, to save the installed packages as requirements.

## Branching
- master is the deployed production branch. Only merge into master if code is tested and production ready.
- develop is the development branch and is not deployed. Use this branch and feature branches to develop.

# Deployment
- When pushing a commit to the master branch, the function get redeployed.
- The function is deployed via [Serverless](https://serverless.com) to an AWS Lambda Function (name in console: serverless-flask-dev-app) with an API Gateway and a DynamoDB table to store data in NoSQL format.
- The base URL of the API is: https://whmhylral1.execute-api.eu-central-1.amazonaws.com/production/ .

# Interesting Learnings
- AWS Lambda has a max. size of 50MB of code, which makes it impractical for machine learning deployment. It's best to know this before you build a CI/CD pipline :).