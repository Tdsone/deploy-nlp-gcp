class DBEntry:

    def __init__(self, question, solution, topic):
        self.question = question
        self.solution = solution
        self.topic = topic

    def toDict(self):
        return {
            "question": self.question,
            "solution": self.solution,
            "topic": self.topic
        }


questions_table = [
    DBEntry("Why is the earth round?",
            "The earth is round because it was a liquid turing lava ball and gravitational forces did the rest", "geography"),
    DBEntry("How did the Alps come into existence", "No idea", "geography"),
    DBEntry("Why do continents exist?",
            "Earth was once one big continent which broke up.", "geography"),
    DBEntry("What is climate change?",
            "Something we should stop.", "geography"),
    DBEntry("Do you like geography questions?",
            "There is no good answer to this question.", "geography"),
    DBEntry("Why did Napoleon conquer Germany?",
            "Because he could.", "history"),
]
